Lession-1 : Introduction [ Completed ]

Lession-2 : AWS Overview [ Completed ]

Lession-3 : Designing highly available, cost-efficient, fault-tolerant, scalable systems [Pending]

- CloudWatch   [Pending]
- CloudTrail   [Pending]
- Config       [Pending]

Lession-4 : IAM [Pending]
    - Roles


Lession-5 : VPC [Completed]

Lession-6 : EC2 [Completed] 
 - Lambada                  [Larners want to recap ]
 - Elastic BeanStalk        [Larners want to recap ]

Lession-7 : S3 [ In-Progress ]

Lession-8 : Route53 [Completed]

Lession-9 : Databases [Completed]

Lession-10 : Application Services [Pending]

Lession-11 : Security Practices for Optimum Cloud Deployment [Theory is Pending]

Lession-12 : Disater Recovery [ Theory is Pending ]

Lession-13 : Troubleshooting [Pending]

Lession-14 : Exam [Pending]